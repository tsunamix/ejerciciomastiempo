package ejercicio;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hotelbeds.supplierintegrations.hackertest.detector.Action;
import com.hotelbeds.supplierintegrations.hackertest.detector.Message;
import com.hotelbeds.utils.Tools;

public class MainTest {

	@Test
	public void com_hotelbeds_supplierintegrations_hackertest_detector_message(){
		
		String sString="";
		
		sString = null;
		assertEquals(null
				,Message.getInstance(sString));
		
		
		sString = "";
		assertEquals(null
				,Message.getInstance(sString));
		
		sString = "10.162.10.75,1336129471,SIGNING_SUCCESS,Will.Smith";
		
		assertNotEquals(null
				,Message.getInstance(sString));
		
		sString = "10.162.1110.75,1336129471,SIGNING_SUCCESS,Will.Smith";
		assertEquals(null
				,Message.getInstance(sString));
		
		sString = "10.162.10.75,1336129471,SIGNIDG_SUCCESS,Will.Smith";
		assertEquals(null
				,Message.getInstance(sString));
		
	}
	
	@Test
	public void com_hotelbeds_tools_isValidateUsername(){
		String sString="";
		
		sString = null;
		assertEquals(false
				,Tools.isValidateUsername(sString));
		
		sString = "";
		assertEquals(false
				,Tools.isValidateUsername(sString));
		
		sString = "hotelbeds";
		assertEquals(true
				,Tools.isValidateUsername(sString));
		
	}
	
	@Test
	public void com_hotelbeds_tools_isValidateIPAddress(){
		
		String sString="";
		
		sString = null;
		assertEquals(false
				,Tools.isValidateIPAddress(sString));
		
		sString = "";
		assertEquals(false
				,Tools.isValidateIPAddress(sString));
		
		sString = "hotelbeds";
		assertEquals(false
				,Tools.isValidateIPAddress(sString));
		
		sString = "8.8.8.8";
		assertEquals(true,
				Tools.isValidateIPAddress(sString));
		
		sString = "8.8.8.8213";
		assertEquals(false,
				Tools.isValidateIPAddress(sString));
	}
	
	@Test
	public void com_hotelbeds_tools_StringToEnumValue(){
		
		String sString="";
		
		sString = null;
		assertEquals(null,
				Tools.StringToEnumValue(Action.class, sString));
		
		sString = "";
		assertEquals(null,
				Tools.StringToEnumValue(Action.class, sString));
		
		sString = "SIGNING_FAILURE";
		assertEquals(Action.SIGNING_FAILURE,
				Tools.StringToEnumValue(Action.class, sString));
		
		sString = "SIGNING_SUCCESS";
		assertEquals(Action.SIGNING_SUCCESS,
				Tools.StringToEnumValue(Action.class, sString));
	}
	
	@Test
	public void com_hotelbeds_tools_isValidateEnumValue(){
		
		
		String sString="";
		
		sString = null;
		assertEquals(false,
				Tools.isValidateEnumValue(Action.class, sString));
		
		sString = "";
		assertEquals(false,
				Tools.isValidateEnumValue(Action.class, sString));
		
		sString = "hotelbeds";
		assertEquals(false,
				Tools.isValidateEnumValue(Action.class, sString));
		
		sString = "SIGNING_FAILURE";
		assertEquals(true,
				Tools.isValidateEnumValue(Action.class, sString));
		
		sString = "SIGNING_SUCCESS";
		assertEquals(true,
				Tools.isValidateEnumValue(Action.class, sString));
		
		
	}

}
