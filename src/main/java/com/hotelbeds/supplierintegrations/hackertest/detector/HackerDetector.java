package com.hotelbeds.supplierintegrations.hackertest.detector;

/**
* 
* Main interface of the exercise
* 
* @author Daniel Garcia <danielgarciagomez@gmail.com>
* @version 1.0
*/

public interface HackerDetector {
	String parseLine(String line);
}
