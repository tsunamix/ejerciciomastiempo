package com.hotelbeds.supplierintegrations.hackertest.detector;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.hotelbeds.sandbox.MainHackerDetector;
import com.hotelbeds.utils.Tools;

/**
* 
* Stack of messages for each IP address. It contain all the message form a IP and check
* if there are valid
* 
* @author Daniel Garcia <danielgarciagomez@gmail.com>
* @version 1.0
*/


public class LogsStack implements Serializable {

	//Constant, how many times for denial request
	private final int LIMIT_TIMES = 5;
	
	//Constant, how many seconds for that limit times
	private final int LIMIT_SECONDS = 5 * 60;
	
	//And List for keep all the message save
	private List<Message> _messages = new ArrayList<Message>();
	
	/**
	 * Add the message to the list and sort it compare all date log, from more closer to far
	 * 
	 * @param message.
	 * @return Nothing.
	**/
	
	public void add(Message m){
		this._messages.add(m);
		Collections.sort(this._messages, new MessageCustomComparator());
	}
	
	/**
	 * Check if there are any message in the stack that launch a denial access
	 * 
	 * @param message.
	 * @return boolean.
	**/
	
	public boolean isValid(){
		
		if (_messages.size() >= LIMIT_TIMES){
		
			MainHackerDetector.logger.info(String.format("--> %d messages from that IP, checking the stack",LIMIT_TIMES));
			
			Date now=new Date();
			long lNow = Tools.DateToEpoch(now);
			long lLimit = lNow - LIMIT_SECONDS;
			
			int iElement=0;
						
			for (Message m: this._messages){
			
				if (m.getDate() <= lNow && m.getDate() >= lLimit){
					MainHackerDetector.logger.info(String.format("Access detected within %d seconds",LIMIT_SECONDS));
					iElement++;
				}
				else{
					MainHackerDetector.logger.info(String.format("Access detected far from %d seconds",LIMIT_SECONDS));
					break;
				}
			}
			
			if (iElement >= LIMIT_TIMES){
				MainHackerDetector.logger.info("Threat has been detected");
				return false;
			}

		}
		
		return true;
	}
}
