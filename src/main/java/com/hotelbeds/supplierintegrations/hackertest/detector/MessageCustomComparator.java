package com.hotelbeds.supplierintegrations.hackertest.detector;

import java.util.Comparator;

/**
* 
* Class helper form sort a List of messages
* 
* @author Daniel Garcia <danielgarciagomez@gmail.com>
* @version 1.0
*/

public class MessageCustomComparator implements Comparator<Message> {
    public int compare(Message o1, Message o2) {
    	return o1.getDate() < o2.getDate() == true ? 1 : 0;
    }
}

