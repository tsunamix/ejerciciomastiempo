package com.hotelbeds.supplierintegrations.hackertest.detector;

import java.io.IOException;
import java.util.Hashtable;

import net.spy.memcached.AddrUtil;
import net.spy.memcached.ConnectionFactoryBuilder;
import net.spy.memcached.MemcachedClient;
import net.spy.memcached.auth.AuthDescriptor;
import net.spy.memcached.auth.PlainCallbackHandler;

import com.hotelbeds.sandbox.MainHackerDetector;
import com.hotelbeds.utils.Tools;

/**
* 
* This is my facade class that implements the HackerDetector interface. This class add all 
* the logical layer for look for laste IP access and save it at memory
* 
* @author Daniel Garcia <danielgarciagomez@gmail.com>
* @version 1.0
*/

public class HackerDetectorEngine implements HackerDetector{

	/**
	 * 
	 * Client remote memcache
	 * 
	**/
	
	private MemcachedClient mc = null;
	
	/**
	 * 
	 * Init all memcache configuration, the keys are hardcore, sorry :(
	 * 
	**/
	
	public HackerDetectorEngine(){
		
		String[] conf = { "PLAIN" };
		PlainCallbackHandler credential=new PlainCallbackHandler("40ac0b","4d5d75a4c5");
		
	    AuthDescriptor ad = new AuthDescriptor(conf,credential);

	    try {
	      mc = new MemcachedClient(
	          new ConnectionFactoryBuilder()
	              .setProtocol(ConnectionFactoryBuilder.Protocol.BINARY)
	              .setAuthDescriptor(ad).build(),
	              AddrUtil.getAddresses("mc1.dev.eu.ec2.memcachier.com:11211")
              );
	      
	      
	    } catch (IOException ioe) {
	    	MainHackerDetector.logger.error("Couldn't create a connection to MemCachier");
	    }
		 
	    
	}

	/**
	 * Check a log line and apply all the logical behabiuor for determinate if is valid or not
	 * 
	 * @param line line of log.
	 * @return NULL if is valid or String if the IP address attacker.
	**/
	
	public String parseLine(String line) {
		
		//Parse the log line to a Message class, I love object�oriented!!
		Message m = Message.getInstance(line);
		
		//Check if it valid and if its action is failure
		if (m != null && m.getAction() == Action.SIGNING_FAILURE){
			
			//Create a hash with a long number, more faster than a string in our hastable
			long hashIp = Tools.ipToLong(m.getIP());
			
			//Get actual hashtable value
			Object o = mc.get(String.valueOf(hashIp));
			
			LogsStack stack = null;
			
			//It's your first time or not?
			if (o == null)
				stack = new LogsStack();
			else
				stack = (LogsStack)o;
				
			//Add the message to LogsStack
			stack.add(m);
			
			//And save to the hashtable again
			mc.set(String.valueOf(hashIp), 0, stack);
			
			//Check if there is a invalid access in logStack for that IP address
			if ( !stack.isValid() )
				return m.getIP();
			
		}
		
		return null;
	}
}
