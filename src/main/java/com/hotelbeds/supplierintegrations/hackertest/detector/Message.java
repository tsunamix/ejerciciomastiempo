package com.hotelbeds.supplierintegrations.hackertest.detector;

import java.io.Serializable;
import java.util.Date;

import com.hotelbeds.sandbox.MainHackerDetector;
import com.hotelbeds.utils.Tools;

/**
* 
* This is my facade class that implements the HackerDetector interface. This class add all 
* the logical layer for look for laste IP access and save it at memory
* 
* @author Daniel Garcia <danielgarciagomez@gmail.com>
* @version 1.0
*/

public class Message implements Serializable {

	//Attribute values
	private String _IP;
	private long _date;
	private Action _action;
	private String _username;
	
	//Setters and getters methods
	public void setIP(String value){
		this._IP=value;
	}
	
	public String getIP(){
		return this._IP;
	}
	
	public void setDate(long value){
		this._date=value;
	}
	
	public long getDate(){
		return this._date;
	}
			
	public void setAction(Action value){
		this._action=value;
	}
	
	public Action getAction(){
		return this._action;
	}
	
	public void setUsername(String value){
		this._username=value;
	}
	
	/**
	 * Parse log lines, and get a object message valid class 
	 * 
	 * @param String Message log.
	 * @return Message object, or a NULL value if there is any problem.
	**/
	
	public static Message getInstance(String sLog){
		
		if (sLog == null)
			return null;
		
		String[] arrayLog = sLog.split(",");
		 
		//Check how many chuck the log line have
		if (arrayLog.length == 4){
			
			String s_IP =  arrayLog[0];
			String s_Date =  arrayLog[1];
			String s_Action =  arrayLog[2];
			String s_Username =  arrayLog[3];
			
			Message m = new Message();
			
			//Valid IP format
			if (Tools.isValidateIPAddress( s_IP) )
				m.setIP(s_IP);
			else{
				MainHackerDetector.logger.error("Not possible to parse IP address");
				return null;
			}
			
			//Valid Date format
			if (Tools.isValidateEpochDate( s_Date ) ){
				m.setDate(Long.parseLong(s_Date));
			}
			else{
				MainHackerDetector.logger.error("Not possible to parse date");
				return null;
			}
			
			//Valid Action format
			if (Tools.isValidateEnumValue(Action.class, s_Action))
				m.setAction(Tools.StringToEnumValue(Action.class, s_Action));
			else{
				MainHackerDetector.logger.error("Not possible to parse action value");
				return null;
			}
			
			//Valid Username format
			if (Tools.isValidateUsername(s_Username))
				m.setUsername(s_Username);
			else{
				MainHackerDetector.logger.error("Not possible to parse username");
				return null;
			}
			
			return m;
			
		}
		else{
			MainHackerDetector.logger.error("Not possible to parse log line");
		}
		
		return null;
		
	}
}
