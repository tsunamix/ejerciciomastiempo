package com.hotelbeds.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tools {
	
	/**
	 * Diff between two RFC date format
	 *
	 * @param  date1 
	 * @param  date2 
	 * @return	minutes between dates
	 * 
	 */
	public static long DiffDateRFCtoDateRFC(String s1, String s2){
		
		Date d1=Tools.RFCtoDate(s1);
		Date d2=Tools.RFCtoDate(s2);
		
		long lMilisegundos = d2.getTime() - d1.getTime();
		long lMinutos = lMilisegundos / (60 * 1000) % 60;
		
		return lMinutos;
	}
	
	/**
	 * Convert RFC date to Java Date
	 *
	 * @param  date
	 * @return	date
	 * 
	 */
	public static Date RFCtoDate(String RFCDate){
		
		String pattern = "EEE, dd MMM yyyy HH:mm:ss Z";
		SimpleDateFormat format = new SimpleDateFormat(pattern, Locale.ENGLISH);
	
		try {
			return format.parse(RFCDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Convert Date to Epoch Date
	 *
	 * @param  date
	 * @return	long
	 * 
	 */
	public static long DateToEpoch(Date date){
		return date.getTime() /1000;
	}
	
	/**
	 * Convert Epoche Date to Date
	 *
	 * @param  string
	 * @return	date
	 * 
	 */
	
	public static Date EpochtoDate(String sEpoch){
		long lEpoch = Long.parseLong( sEpoch );
		return new Date( lEpoch * 1000 );
	}
	
	/**
	 * Convert an IP address, in String mode, to a Long value 
	 *
	 * @param  addr  
	 * @return		the long that represents the IP address
	 * 
	 */
	public static long ipToLong(String addr) { 
        String[] addrArray = addr.split("\\.");

        long num = 0; 
        for (int i = 0; i < addrArray.length; i++) { 
            int power = 3 - i;

            num += ((Integer.parseInt(addrArray[i]) % 256 * Math.pow(256, power))); 
        } 
        return num; 
    }
	
	/**
	 * Check if is valid IP Address
	 *
	 * @param  IP Address
	 * @return	boolean
	 * 
	 */
	
	public static boolean isValidateIPAddress( String ipAddress ){
		
		if (ipAddress==null)
			return false;
		
		final String PATTERN = 
		        "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
		        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
		        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
		        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
		
		Pattern pattern = Pattern.compile(PATTERN);
		Matcher matcher = pattern.matcher(ipAddress);
		return matcher.matches();  
      
	}
	
	/**
	 * Check if is valid Epoch Date
	 *
	 * @param  Epoch
	 * @return	boolean
	 * 
	 */
	
	public static boolean isValidateEpochDate( String sEpoch ){
		boolean isValid=true;
		long lEpoch = Long.parseLong( sEpoch );
		try
		{
			Date date=new Date( lEpoch * 1000 );
		}
		catch(Exception e){
			isValid=false;
		}
		return true;
	}
	
	/**
	 * Check if is valid Username
	 *
	 * @param  username
	 * @return	boolean
	 * 
	 */
	
	public static boolean isValidateUsername( String sUsername ){
		
		if (sUsername == null)
			return false;
		
		if ( sUsername.trim().equals("") )
			return false;
		else
			return true;
	}
	
	/**
	 * Check if the string value is contain in the enum values
	 *
	 * @param  Enum
	 * @param  String
	 * @return	boolean
	 * 
	 */
	
	public static <T extends Enum<T>> boolean isValidateEnumValue(Class<T> c, String string ){
		boolean isValid=true;
		
		if (string==null || string=="")
			return false;
		
		if ( StringToEnumValue(c,string) == null)
			isValid=false;
		
		return isValid;
	}
	
	/**
	 * Convert string in enum value
	 *
	 * @param  Enum
	 * @param  String
	 * @return	Enum
	 * 
	 */
	
	public static <T extends Enum<T>> T StringToEnumValue(Class<T> c, String string)
	{
	    if( c != null && string != null )
	    {
	        try
	        {
	            return Enum.valueOf(c, string.trim().toUpperCase());
	        }
	        catch(IllegalArgumentException ex)
	        {
	        }
	    }
	    return null;
	}
	
}
