package com.hotelbeds.sandbox;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
* 
* This class is made form inject logs sequence to HackerDetectorEngine, It read from json 
* file logs and load them to this factory.
* 
* @author Daniel Garcia <danielgarciagomez@gmail.com>
* @version 1.0
*/

public class LogsFactory {
	
	//And List for keep all the message save
	private List<String> _logs = new ArrayList<String>();
	
	//Setters and getters methods
	public List<String> getLogs(){
		return this._logs;
	}
	
	public List<String> setLogs(){
		return this._logs;
	}
	
	
	/**
	 * static methods that creates the factory and fills it with all the json logs data
	 * 
	 * @param nothing
	 * @return LogsFactory object
	**/
	
	public static LogsFactory getInstance(){
		
		JSONParser parser = new JSONParser();
		
		LogsFactory lf = new LogsFactory();
		
		try {
			Object obj = parser.parse(new FileReader(ClassLoader.getSystemResource("logs.json").getFile()));
			JSONObject jsonObject = (JSONObject) obj; 
			
			JSONArray msg = (JSONArray) jsonObject.get("logs");
			Iterator<String> iterator = msg.iterator();
			while (iterator.hasNext()) {
				lf.setLogs().add(iterator.next());
			}
			
		} catch (IOException e) {
			MainHackerDetector.logger.fatal("Imposible encontrar fichero logs.json");
			return null;
		}
		catch (Exception e){
			MainHackerDetector.logger.fatal("Imposible cargar los logs de prueba");
			return null;
		}
		
		return lf;
	}
	
	public LogsFactory(){	
	}
}
