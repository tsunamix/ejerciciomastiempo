package com.hotelbeds.sandbox;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.hotelbeds.supplierintegrations.hackertest.detector.HackerDetectorEngine;

/**
* 
* This is my main class. I simulate all the test behaviors. Thanks LogsFactory I am going 
* in inject into the HackerDetectEngine some logs lines. 
* 
* @author Daniel Garcia <danielgarciagomez@gmail.com>
* @version 1.0
*/


public class MainHackerDetector {

	public static final Logger logger = Logger.getLogger(MainHackerDetector.class);
	
	public static void main(String[] args) {
		//Start setting log, for audit all event in the system
		//PropertyConfigurator.configure(ClassLoader.getSystemResource("log4j.properties").getFile());
		
		//Start the singleton facade the implements our interface HackerDetector
		HackerDetectorEngine hde = new HackerDetectorEngine();
		
		
		
		//Start logs fake factory, for make some real test, and check all the behaviors, this
		//Factory retrive its logs from /resources/logs.json, please check it
		LogsFactory lf = LogsFactory.getInstance();
		
		
		if (lf != null)
		{
			//Retrive all fake 
			for (String log : lf.getLogs())  
			{
				logger.info(String.format("Check new log : '%s'", log));
				
				//Sending log to our system, check if it is valid or nor
				String sIp=hde.parseLine(log);
				
				if (sIp != null){
					logger.info( String.format("Threat has been detected from %s",sIp) );
				}
				else{
					logger.info("No threat, is OK");
				}
			}
		}
		
	}

}
