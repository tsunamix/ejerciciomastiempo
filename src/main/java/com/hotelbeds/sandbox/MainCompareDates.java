package com.hotelbeds.sandbox;

import com.hotelbeds.utils.Tools;

public class MainCompareDates {

	public static void main(String[] args) {
		
		String s1 = "Sun, 14 Mar 2013 10:29:05 -0200";
		String s2 = "Sun, 14 Mar 2013 10:37:00 -0200";
		
		long minutes=Tools.DiffDateRFCtoDateRFC(s1, s2);
		System.out.print(String.format("Hay %d minutos de diferencia",minutes));

	}

}
