## ¿Esto qué es?

Se trata del ejercicio en plan examen unversitario, pero esta vez se utiliza para evaluar a posibles candidatos a un puesto de trabajo...

Curiosamente, tuve la oportunidad de realizar estas pruebas a algunos candidatos de programadores hacer ya tiempo en mi empresa de Bilbao... en general tuve muy buenos resultados, aunque me llevo un mal sabor de boca al ver la poco motivación que algunos jovenes ingenieros tienen... elimino el modo bucolico y continuo...

## Normas

Parece ser que me han dado 24 horas para realizar el ejercicio, que se compone, creo, que de dos subejercicios o entregables... El tiempo de ejecución del ejercicio no puede exceder de 1 hora, y al no decir nada, entiendo que puedo usar Google si tengo dudas.

Me imagino que en hora podré hacer algo, pero prometo sacar una segunda versión más extendida, y asi sacamos más jugo al tema...

## Ejercicio 1: Sistema de detección de Hackers


El sistema de la compañía permite a los clientes conectarse utilizando un usuario y un password.

Tenemos el requisito de desarrollar un sistema que ayude a identificar intentos de hackear el sistema.

El programa a de procesar los logs de acceso y detectar actividades sospechosas.

Escribe un programa en Java que implemente la interfaz HackerDetector (ver más abajo) que define un único método ‘parseLine’. Este método recibe una línea del log cada vez yretorna la IP sospechosa o null si la actividad parece normal.

```java
package com.hotelbeds.supplierintegrations.hackertest.detector

public interface HackerDetector {
	String parseLine(String line);
}

```
El método parseLine se llamará cada vez que una nueva línea de log se produzca. Las líneas del log tienen el siguiente formato:

ip,fecha,acción,nombre_de_usuario

+ IP sería 10.162.10.75
+ Fecha en formato epoch: 1336129471
+ Acción puede ser: SIGNING_SUCCESS o SIGNING_FAILURE
+ nombre_de_usuario es un cadena como Will.Smith

Una línea de log sería algo parecido a esto:

10.162.10.75,1336129471,SIGNING_SUCCESS,Will.Smith

El método de detección consistirá en identificar aquella IP que realice 5 o más intentos fallidos en un período de 5 minutos.

Nuestros logs de acceso pueden generar cerca de 100.000 intentos fallidos al día, por lo que el consumo de memoria debe ser tenido en cuenta.


### Explicación

### Instrucciones

Voy a trabajar con Maven, así que esto simplifica enormamente todo el proceso de despligue dependencias, pruebas unitarias y demás. Simplemente una vez hecho el clone del repositorio en local

```bash

#Instalar
mvn install

#Para compilar todo el proyecto
mvn compile

#Para lanzar las pruebas unitarias
mvn test

#Para ejecutar el
mvn exec:java -Dexec.mainClass="com.hotelbeds.sandbox.MainHackerDetector"

```


### TODO

Parece que al final se me ha hecho el tiempo encima y tampoco quiero pasarme con las normas, esta bien programar y definir sistemas potentes, pero más importante es entregarlo a tiempo y en las horas acordadas... A continuación listo algunas cosas pendientes.

+ Sigo pensando que dejo muchas pruebas sin hacer... 



## Ejercicio 2: Cálculo de tiempo
Escribe una función que retorne el número de minutos (redondeado hacia abajo) entre dos timestamps time1 y time2 en formato RFC 2822 (ej: Thu, 21 Dec 2000 16:01:07 +0200). No te olvides de las zonas horarias

### Explicación

El archiconocido enemigo de java.... El manejo de Fechas. En algun momento he utilizado la libería de Mojo para poder hacer una mejor gestión de tiempos, no obstante para este plantamiento creo que se puede hacer con el Date nativo de java

### Instrucciones

Reutilizo el proyecto anterior, ya que he utilizado el package Tools para dejar las funciones importantes

```bash

#Instalar
mvn install

#Para compilar todo el proyecto
mvn compile

#Para lanzar las pruebas unitarias
mvn test

#Para ejecutar el
mvn exec:java -Dexec.mainClass="com.hotelbeds.sandbox.MainCompareDates"

```

Un ejemplo de codigo sería

```java

package com.hotelbeds.sandbox;

import com.hotelbeds.utils.Tools;

public class MainCompareDates {

	public static void main(String[] args) {
		
		String s1 = "Sun, 14 Mar 2013 10:29:05 -0200";
		String s2 = "Sun, 14 Mar 2013 10:37:00 -0200";
		
		long minutes=Tools.DiffDateRFCtoDateRFC(s1, s2);
	}

}

```

